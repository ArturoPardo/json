package es.devcenter.Json;

public class Vehiculo {
	
	private int idVehiculo;
	private String tipo ;

	
	public Vehiculo(int _idVehiculo, String _tipo ) {
		this.idVehiculo = _idVehiculo;
		this.tipo = _tipo;
	}

	public String getTipo() {
		return tipo;
	}



	@Override

	public String toString() {
		return "Vehiculo-> ID: " + idVehiculo + " Tipo: " + tipo + "\n";

	}

	
	
}
