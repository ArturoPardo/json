package es.devcenter.Json;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EjerciciosLinkedLIst {

	public static void main(String arg[]) {

        List<Persona2> listaarray = new ArrayList<Persona2>();
        List<Persona2> listalinked = new LinkedList<Persona2>();

        long antes;
        for(int i=0;i<10000;i++)
        {

           listaarray.add(new Persona2(i,"Persona2"+i,i));  // En este ejemplo cada persona lleva datos ficticios
            listalinked.add(new Persona2(i,"Persona2"+i,i));

        }

        System.out.println("Tiempo invertido en insertar una persona en listaarray (en nanosegundos):");

        antes = System.nanoTime();

        listaarray.add(new Persona2(10001,"Prueba",10001)); // Inserción en posicion 0 de una persona

        System.out.println(System.nanoTime()- antes); 

        System.out.println("Tiempo invertido en insertar una persona en listalinked (en nanosegundos):");

        antes = System.nanoTime();

        listalinked.add(new Persona2(10001,"Prueba",10001));  // Inserción en posicion 0 de una persona

        System.out.println(System.nanoTime()- antes);

  } 
}
