package es.devcenter.Json;

public class Persona2 {

	private int idPersona;
	private String nombre;
	private int altura;

	public Persona2(int idPersona, String nombre, int altura) {

		this.idPersona = idPersona;
		this.nombre = nombre;
		this.altura = altura;
	}

	@Override

	public String toString() {
		return "Persona-> ID: " + idPersona + " Nombre: " + nombre + " Altura: " + altura + "\n";

	}
}
