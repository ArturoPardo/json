package es.devcenter.Json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ProyectoVehiculo {

	public static void main(String[] args) {

		List<Vehiculo> listaarray = new ArrayList<Vehiculo>();

		for (int i = 0; i < 12; i++) {

			listaarray.add(i, new Vehiculo(i, miRandom())); // En este ejemplo cada persona lleva datos ficticios

		}
		
		
		ProyectoVehiculo.sacarArrays( listaarray);

		Iterator<Vehiculo> vehiculosIterador = listaarray.iterator();
		while (vehiculosIterador.hasNext()) {
			Vehiculo vehiculo = vehiculosIterador.next();

			if (!vehiculo.getTipo().equals("coche")) {
				vehiculosIterador.remove();
			}

		}
		System.out.println("\n");
		ProyectoVehiculo.sacarArrays( listaarray);
	}

	
	public static String miRandom() {
		String[] tipo = { "coche", "camion", "moto", "furgoneta" };
		int numRandon = (int) Math.round(Math.random() * 3);
		String MiValor = tipo[numRandon];
		return MiValor;
		

	}

	public static void sacarArrays(List<Vehiculo> vehiculos) {

		int numeroCoches = 0;
		int numeroCamion = 0;
		int numeroFurgoneta = 0;
		int numeroMoto = 0;
		int totalBorrados =0;
		for (Vehiculo v : vehiculos) {
			switch (v.getTipo()) {
			case "coche":
				numeroCoches++;
				break;
			case "camion":
				numeroCamion++;
				break;
			case "furgoneta":
				numeroFurgoneta++;
				break;
			case "moto":
				numeroMoto++;
				break;

			}

		}

		System.out.println("numeroMoto " + numeroMoto);
		System.out.println("numeroCamion " + numeroCamion);
		System.out.println("numeroFurgoneta " + numeroFurgoneta);
		System.out.println("numeroCoche " + numeroCoches);
		totalBorrados = numeroMoto + numeroCamion + numeroFurgoneta;
		System.out.println("Borrados " +totalBorrados);
		rellenarArray(totalBorrados);

	}
	
	public static void rellenarArray( int totalBorrados) {
		System.out.println(totalBorrados);
		
		
		
	}
	

}
